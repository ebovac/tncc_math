default: mtfigures

pin_at_pars.png pin_at_pars.pdf pin_at_pars.tiff: pin_control_prep.rds control_limits.rda

stop_at_pars.png stop_at_pars.pdf stop_at_pars.tiff: rho_control_prep.rds control_limits.rda

SI_sensitivity_%.png: SI_sensitivity.R alt_sensitivity_shared.rda
	Rscript $^ SI_sensitivity.png

alt_SI_sensitivity_%.png: alt_SI_sensitivity.R alt_sensitivity_shared.rda
	Rscript $^ alt_SI_sensitivity.png

SI_secneg_sensitivity_%.png: SI_secneg_sensitivity.R alt_sensitivity_shared.rda
	Rscript $^ SI_secneg_sensitivity.png

SIfigs: SI_sensitivity_1.png SI_secneg_sensitivity_1.png

slide_pin_at_pars.png: pin_control_prep.rds control_limits.rda

slide_stop_at_pars.png: rho_control_prep.rds control_limits.rda

%.pdf %.tiff %.jpg %.png: %.R plot_parts.rda
	Rscript $^ $@

#  %.tiff %.jpg %.png also work

hybriddesign.png: design_comparison.rds

pin_control_prep.rds: plot_parts.rda spectrum.rds

rho_control_prep.rds: plot_parts.rda spectrum.rds

control_limits.rda: pin_control_prep.rds rho_control_prep.rds

alt_sensitivity_shared.rda sensitivity_shared.rda: plot_parts.rda

%.rda %.rds: %.R
	Rscript $^ $@

.PRECIOUS: %.rda

FNAMES := secondarynegonly pin_sensitivity relaxGconstraint Gconstraint Rconstraint monotonicity basicscenarios pin_at_pars stop_at_pars
PNGFIGS := $(addsuffix .png,$(FNAMES))
PDFFIGS := $(addsuffix .pdf,$(FNAMES))
TIFFIGS := $(addsuffix .tiff,$(FNAMES))

mtfigures: $(addsuffix .pdf,basicscenarios secondarynegonly pin_sensitivity pin_at_pars stop_at_pars)

veepedfigures: $(addsuffix .png,pin_at_pars stop_at_pars)

figures: $(PNGFIGS)

tiffigures: $(TIFFIGS)

pdffigures: $(PDFFIGS)

clean:
	rm -f $(PNGFIGS)
	rm -f $(PDFFIGS)

%-trim.svg: %.svg
	# remove the viewBox attribute
	# remove the first clip element (i.e., the slide background)

fig1.eps fig2.eps: fig1.svg fig2.svg
	inkscape -E fig1.eps fig1.svg
	inkscape -E fig2.eps fig2.svg

fig3.tif fig4.tif fig5.tif fig6.tif fig7.tif: basicscenarios.tiff pin_sensitivity.tiff secondarynegonly.tiff pin_at_pars.tiff stop_at_pars.tiff
	tiffcp -c lzw basicscenarios.tiff fig3.tif
	tiffcp -c lzw pin_sensitivity.tiff fig4.tif
	tiffcp -c lzw secondarynegonly.tiff fig5.tif
	tiffcp -c lzw pin_at_pars.tiff fig6.tif
	tiffcp -c lzw stop_at_pars.tiff fig7.tif

manufigs: fig1.eps fig2.eps $(patsubst %,fig%.tif,3 4 5 6 7)