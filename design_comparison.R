
suppressPackageStartupMessages({
  require(data.table)
})

.args <- c("design_comparison.rds")
.args <- commandArgs(trailingOnly = T)

drawevent <- function(rseed, PnegFraction, L, E, pin, lambda, pt) {
  set.seed(rseed)
  
  # assume primary test neg process unbiased
  prinegs <- rgeom(1, 1-PnegFraction)
  prinegvac <- rbinom(1, prinegs, L*pin)
  prinegnon <- prinegs - prinegvac
  
  priposparticipant <- runif(1) < ((1-L*E)*pin/(1-L*E*pin)) # equation (6) in SI
  
  # assume high risk contact rate independent of participation
  highriskcons <- rpois(1, lambda)
  
  if (priposparticipant) {
    priposvac <- as.integer(runif(1) < ((1-E)*L/(1-L*E))) # equation (4) in SI
    
    secondary <- rbinom(1, highriskcons, pt*(1-L*E))
    secneg <- highriskcons - secondary
    secnegvac <- rbinom(1, secneg, L*(1-(1-E)*pt)/(1-(1-L*E)*pt))
    secnegnon <- secneg - secnegvac
    secposvac <- rbinom(1, secondary, ((1-E)*L/(1-L*E)))
    secposnon <- secondary - secposvac
  } else {
    priposvac <- 0
    secondary <- rbinom(1, highriskcons, pt)
    secnegnon <- highriskcons - secondary
    secnegvac <- 0
    secposnon <- secondary
    secposvac <- 0
  }
  priposnon <- 1 - priposvac
  
  return(c(
    prinegvac = prinegvac,
    prinegnon = prinegnon,
    priposvac = priposvac,
    priposnon = priposnon,
    secnegvac = secnegvac,
    secnegnon = secnegnon,
    secposvac = secposvac,
    secposnon = secposnon
  ))
}

SIMN <- 100000

plot.grid <- data.table(expand.grid(
  sample_id = 1:SIMN,
  PnegFraction = 0.9,
  StoPratio = 0.1,
  L = 0.7,
  pin = c(0.6, 0.8, 1.0),
  pt = 0.5,
  E = c(0.01, 0.2, 0.4, 0.6, 0.8, 0.99)
))

setkeyv(plot.grid, colnames(plot.grid))

# B/(B+1) = PnegFraction; B = PnegFraction/(1-PnegFraction)
plot.grid[, lambda := StoPratio * PnegFraction/(1-PnegFraction) ]

res <- plot.grid[, as.list(drawevent(sample_id, PnegFraction, L, E, pin, lambda, pt)), by=key(plot.grid)]

rm(plot.grid)

############ make a sample of trials ####################
NUMTRIALS <- 1000 # how many trial samples to do
TAKE <- 1000 # how many index cases - seems plausible, for the full duration of DRC

trials <- rbindlist(lapply(1:NUMTRIALS, function(rid) {
  set.seed(rid)
  res[,{
    .SD[sample(.N, TAKE),{
      prionly = 1-(cumsum(priposvac)/cumsum(priposnon))*(cumsum(prinegnon)/cumsum(prinegvac))
      csonly = 1-(cumsum(secposvac)/cumsum(secposvac+secnegvac))*(cumsum(secnegnon+secposnon)/cumsum(secnegnon))
      .(
        sample_id = 1:TAKE,
        plainTNCC = 1-(cumsum(priposvac+secposvac)/cumsum(priposnon+secposnon))*(cumsum(prinegnon+secnegnon)/cumsum(prinegvac+secnegvac)),
        prionlyTNCC = prionly,
        CSonly = 1-(cumsum(secposvac)/cumsum(secposvac+secnegvac))*(cumsum(secnegnon+secposnon)/cumsum(secnegnon)),
        hybrid = 0.5*prionly + 0.5*csonly,
        trial_id = rid
      )
    }]
  }, by=setdiff(key(res), "sample_id"), .SDcols=-c("sample_id")]
}))

setkeyv(trials, c(key(res),"trial_id"))

rm(res)

saveRDS(trials, tail(.args, 1))