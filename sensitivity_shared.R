suppressPackageStartupMessages({
  require(data.table)
  require(ggplot2)
})

.args <- c("plot_parts.rda", "sensitivity_shared.rda")
.args <- commandArgs(trailingOnly = T)

load(.args[1])

# at least half of primary recruiting is test-negative
fnegset <- c(0.5, 0.75, 0.9)
# at most, same amount of secondary recruitment as primary recruitment
rhofracset <- c(0.1, 0.25, 0.5)

setpoints <- c(.99, .9, 0.75, 0.5, 0.25, .1, .01)

testset <- data.table(expand.grid(
  pin = setpoints, pt = setpoints,
  fneg = fnegset, rhofrac = rhofracset
))[,
  expand.grid(E=seq(0,1,by=0.01), L=seq(0,1,by=0.01))
  , by=.(pin, pt, fneg, rho=rhofrac/(1-rhofrac))
]

geom_heatcontour <- function(
  contour_var, contour_levels,
  raster_alpha = 0.75,
  midpoint = median(contour_levels),
  contour_order = sort(contour_levels[which(contour_levels >= midpoint)]),
  contour_filter = function(dt) dt
) {
  list(
    geom_raster(aes(fill={{contour_var}} - midpoint), alpha = raster_alpha),
    geom_contour(
      aes(
        z = {{contour_var}} - midpoint,
        color = factor(
          sign(stat(level)),
          levels = c("1","0","-1"),
          ordered = T
        ),
        linetype = factor(
          abs(stat(level)),
          levels = as.character(contour_order),
          ordered = T
        )
      ),
      data = contour_filter,
      breaks = contour_levels
    )
  )
}

scale_heatcontour <- function(
  name, contour_levels,
  direction_name = paste(name, "Direction", sep=ifelse(is.null(title.position) | title.position %in% c("left","right"),"\n"," ")),
  direction_labels = c("Positive", "Neutral", "Negative"),
  direction_colors = c("firebrick", "black", "dodgerblue"),
  level_name = paste(name, "Magnitude", sep=ifelse(is.null(title.position) | title.position %in% c("left","right"),"\n"," ")),
  midpoint = median(contour_levels),
  limits = range(contour_levels),
  low = muted("blue"), mid = "white", high = muted("red"),
  lty_values = c(3, 4, 2, 6, 5, 1),
  title.position = NULL,
  barwidth = NULL, barheight = NULL
) {
  list(
    scale_color_manual(
      direction_name,
      labels = direction_labels,
      values = direction_colors,
      guide = guide_legend(
        title.position = title.position,
        label.position = "top"
      )
    ),
    scale_linetype_manual(
      level_name,
      values = lty_values, drop = F,
      guide = guide_legend(
        title.position = title.position,
        override.aes = list(color="grey30"),
        label.position = "top",
        keywidth = 3
      )
    ),
    scale_fill_gradient2(
      name = name,
      breaks = contour_levels,
      low=low, mid=mid, high=high,
      midpoint = midpoint,
      limits = limits,
      guide = guide_colorbar(
        title.position = title.position,
        barwidth = barwidth,
        barheight = barheight
      )
    )
  )
}


scale_x_efficacy <- function(
  name = expression("True Efficacy, "*italic(E)),
  breaks = c(0.1, 0.5, 0.9),
  expand = c(0, 0),
  ...
) scale_x_continuous(
  name = name,
  breaks = breaks,
  expand = expand,
  ...
)

scale_y_coverage <- function(
  name = expression("Intervention Coverage in Targeted Population, "*italic(L)),
  breaks = c(0.1, 0.5, 0.9),
  expand = c(0, 0),
  ...
) scale_y_continuous(
  name = name,
  breaks = breaks,
  expand = expand,
  ...
)

testset[order(pin), pinlab := factor(pin, ordered = T) ]
levels(testset$pinlab) <- paste0(c("p['in']*'='*",rep("", length(levels(testset$pinlab)))), levels(testset$pinlab))

testset[order(pt), ptlab := factor(pt, ordered = T) ]
levels(testset$ptlab) <- paste0(c("p['t']*'='*",rep("", length(levels(testset$ptlab)))), levels(testset$ptlab))

testset[order(fneg), fneglab := factor(paste0("f['-']*'='*", fneg), ordered = T)]
testset[order(rho), rholab := factor(paste0("rho*'='*", rho), ordered = T)]

plotter <- function(dt) ggplot(dt) + aes(E, L) + facet_nested(
  fneglab + ptlab ~ rholab + pinlab,
  labeller = labeller(
    pinlab = label_parsed, ptlab=label_parsed,
    fneglab = label_parsed, rholab = label_parsed
  )
) +
  geom_heatcontour(
    err,
    c(-.6, -.3, -.1, -.01, 0, .01, .1, .3, .6),
    contour_filter = function(dt) dt[between(E, 0, 1, incbounds = F)]
  ) +
  scale_x_efficacy(
    breaks = c(0, .1, .25, .5, .75, .9, 1),
    labels = function(bs) { bs[which(bs %in% c(0,.25,.75,1))]<-""; bs }
  ) +
  scale_y_coverage(
    breaks = c(0, .1, .25, .5, .75, .9, 1),
    labels = function(bs) { bs[which(bs %in% c(0,.25,.75,1))]<-""; bs }
  ) +
  scale_heatcontour(
    "Effectiveness Error",
    round(seq(-0.9, 0.9, by=0.3), 1),
    direction_labels = c("Underestimate", "No Bias", "Overestimate"),
    title.position = "top",
    barwidth = 15,
    low = muted("red"), high = muted("blue"),
    direction_colors = rev(c("firebrick", "black", "dodgerblue"))
  ) +
  theme_minimal() +
  theme(
    #plot.background = element_rect(fill="gray90"),
    panel.background = element_rect(fill=NA, color="black", size = 0.5),
    legend.position = "bottom",
    axis.ticks = element_line()
  )

frames <- data.table(expand.grid(fncmp=fnegset, rhocmp=rhofracset/(1-rhofracset)))

save(list = ls(), file = tail(.args, 1))